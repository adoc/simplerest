"""A very simple Restful API.

IP
--
10.2.2.201:5000


URIs
----
/                   (GET)       Simple Hello World
/apiv1/record       (GET)       Get all records (record collection)
/apiv1/record       (POST)      Create a record, record with generated id is returned
/apiv1/record/{id}  (GET)       Get a record given the {id}
                    (PUT)       Update a record given the {id} and the request payload.
                    (DELETE)    Delete a record given the {id}

"""


# import json
from uuid import uuid4
from bson import json_util as json
from flask import Flask, Response, request, abort, send_from_directory
from pymongo import MongoClient


app = Flask(__name__, static_folder="static", static_path="")


client = None
db = None
col = None


def setup_mongo():
    """Setup MongoDB client, db and collection.
    """

    global client, db, col
    client = MongoClient()
    db = client['restful_play_db']
    col = db['apitest_records']


def db_get_all():
    """Get all records from the mongo collection.
    """

    return col.find({}, {'_id': False})


def db_get_one(uuid):
    """Get one record from the mongo collection.
    """

    return col.find_one({'id': str(uuid)}, {'_id': False})


def db_insert(doc):
    """Insert a record in to the mongo collection. Generates a random UUID as the record id.
    """

    doc.update(doc, {'id': str(uuid4())})
    col.insert_one(doc)

    return doc


def db_update(uuid, doc):
    """Update a record in the mongo collection.
    """

    # Do not accept an id key in data.
    if 'id' in doc:
        del doc['id']

    return col.update({'id': str(uuid)}, doc)


def db_delete(uuid):
    """Delete a record in the mongo collection given its id. Return the number of records deleted.
    """

    return {'n': col.delete_one({'id': str(uuid)}).deleted_count}


def response(payload):
    """Build a Flask Response object with the JSON mimetype.
    """

    return Response(json.dumps(payload), mimetype='application/json')


@app.route('/apiv1/record', methods=("GET",))
def record_list():
    """Get all records.
    """
    return response(db_get_all())


@app.route('/apiv1/record', methods=("POST",))
def record_create():
    """Create a record.
    """
    # Get POST payload and load the json.
    payload = json.loads(request.data)
    return response(db_insert(payload))


def _record_get(uuid):
    # Get the document
    document = db_get_one(id)
    if not document:
        abort(404)
    else:
        return document


@app.route('/apiv1/record/<string:uuid>', methods=("GET",))
def record_get(uuid):
    """Get a record given the id.
    """
    # Respond with the document, 404 if it doesn't exist.
    return response(_record_get(uuid))


@app.route('/apiv1/record/<string:uuid>', methods=("PUT",))
def record_update(uuid):
    """Update a record given the id.
    """

    # Get the document, 404 if it doesn't exist.
    doc = _record_get(uuid)
    # Get POST payload and load the json.
    payload = json.loads(request.data)
    # Update the document dict with the payload.
    doc.update(payload)

    # Update the database and respond.
    return response(db_update(uuid, doc))


@app.route('/apiv1/record/<string:uuid>', methods=("DELETE",))
def record_delete(uuid):
    """Delete a record given the id.
    """

    # Deletes the record and respond with the 'n' number of records changed.
    return response(db_delete(uuid))


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js/', path)


@app.route('/')
def root():
    return app.send_static_file('html/index.html')


if __name__ == '__main__':
    setup_mongo()
    app.run(host='0.0.0.0', debug=True)
