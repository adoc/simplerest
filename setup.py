from setuptools import setup, find_packages

setup(
    name='simplerest',
    version='',
    description='',
    author='',
    author_email='',
    url='',
    install_requires=["flask", "pymongo"],
    setup_requires=[],
    packages=find_packages(exclude=[]),
    include_package_data=True,
    zip_safe=False,
    paster_plugins=[]
)